﻿/// These tests were created in collaboration between:
///         Scott Wells and Matthew Mendez
/// Class:  CS3500 at the University of Utah
/// Date:   November 24, 2014

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Boggle;
using CustomNetworking;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace BoggleServerTest
{
    /// <summary>
    /// This Tester class is used to test the integrity of the functionality of the BoggleServer class.
    /// 
    /// *****IMPORTANT*****
    ///     The CommanLineArgumentTest opens 2 Consoles when run. These need to be closed by hand to get the 
    ///     proper test results back!!! It will not close on its own and it is not waiting for anything else.
    /// 
    /// </summary>
    [TestClass]
    public class ServerTests
    {
        private string player1StartMessage;
        private string player2StartMessage;
        private string player1TimeMessage;
        private string player2TimeMessage;
        private string player1SummaryMessage;
        private string player2SummaryMessage;
        private string player1Score;
        private string player2Score;
        private StringSocket player1;
        private StringSocket player2;
        private readonly Object threadkey = new Object();


        /// <summary>
        /// Tests that the server sends START BOARD TIME OPPONENT_NAME when both
        /// players have sent in their names.
        /// </summary>
        [TestMethod]
        public void StartMessageTest()
        {
            BoggleServer bs = new BoggleServer(5, "dictionary.txt", "LOVEBOGGLESERVER", 2001);
            TcpClient client1 = new TcpClient("localhost", 2001);
            player1 = new StringSocket(client1.Client, UTF8Encoding.Default);
            TcpClient client2 = new TcpClient("localhost", 2001);
            player2 = new StringSocket(client2.Client, UTF8Encoding.Default);
            player1.BeginSend("PLAY player1\n", (e, o) => { }, null);
            player2.BeginSend("PLAY player2\n", (e, o) => { }, null);
            player1.BeginReceive(StartReceive, player1);
            player2.BeginReceive(StartReceive, player2);
            Thread.Sleep(1000);
            Assert.AreEqual("START LOVEBOGGLESERVER 5 player2", player1StartMessage);
            Assert.AreEqual("START LOVEBOGGLESERVER 5 player1", player2StartMessage);
            player1.Close();
            player2.Close();
            client1.Close();
            client2.Close();
        }


        /// <summary>
        /// Tests that the timer sends the correct amount of time updates and that the
        /// game summary 5 zeros preceded by "STOP".
        /// </summary>
        [TestMethod]
        public void TimerAccuracyTest()
        {
            BoggleServer bs = new BoggleServer(5, "dictionary.txt", "LOVEBOGGLESERVER", 6666);
            TcpClient client1 = new TcpClient("localhost", 6666);
            player1 = new StringSocket(client1.Client, UTF8Encoding.Default);
            TcpClient client2 = new TcpClient("localhost", 6666);
            player2 = new StringSocket(client2.Client, UTF8Encoding.Default);
            player1.BeginSend("PLAY player1\n", (e, o) => { }, null);
            player2.BeginSend("PLAY player2\n", (e, o) => { }, null);
            player1.BeginReceive((s, e, o) => { }, player1);
            player2.BeginReceive((s, e, o) => { }, player2);
            for (int i = 0; i < 5; i++)
            {
                player1.BeginReceive(TimeReceive, player1);
                player2.BeginReceive(TimeReceive, player2);
            }
            player1.BeginReceive(SummaryReceive, player1);
            player2.BeginReceive(SummaryReceive, player2);
            Thread.Sleep(6000);
            Assert.AreEqual("TIME 5TIME 4TIME 3TIME 2TIME 1", player1TimeMessage);
            Assert.AreEqual("TIME 5TIME 4TIME 3TIME 2TIME 1", player2TimeMessage);
            Assert.AreEqual("STOP 0 0 0 0 0", player1SummaryMessage);
            Assert.AreEqual("STOP 0 0 0 0 0", player2SummaryMessage);
        }

        /// <summary>
        /// Tests that the server computes the correct scores for the game
        /// and handles all cases of incorrect user input.
        /// </summary>
        [TestMethod]
        public void ComputedScoresTest()
        {
            int timeBetweenSends = 250;
            BoggleServer bs = new BoggleServer(10, "dictionary.txt", "RESTARUANTSHYPPA", 7777);
            TcpClient client1 = new TcpClient("localhost", 7777);
            player1 = new StringSocket(client1.Client, UTF8Encoding.Default);
            TcpClient client2 = new TcpClient("localhost", 7777);
            player2 = new StringSocket(client2.Client, UTF8Encoding.Default);
            player1.BeginSend("jibberish\n", (e, o) => { }, null);
            player1.BeginReceive(IgnoringReceive, null);
            Thread.Sleep(500);
            player1.BeginSend("PLAY player1\n", (e, o) => { }, null);
            player2.BeginSend("PLAY player2\n", (e, o) => { }, null);
            player1.BeginReceive((s, e, o) => { }, player1);
            player2.BeginReceive((s, e, o) => { }, player2);
            Thread.Sleep(500);
            player1.BeginSend("WORD restaurant\n", (e, o) => { }, null);
            Thread.Sleep(timeBetweenSends);
            runReceiveScoreCallback();
            player2.BeginSend("WORD RUSH\n", (e, o) => { }, null);
            Thread.Sleep(timeBetweenSends);
            runReceiveScoreCallback();
            player1.BeginSend("WORD happY\n", (e, o) => { }, null);
            Thread.Sleep(timeBetweenSends);
            runReceiveScoreCallback();
            player1.BeginSend("WORD STares\n", (e, o) => { }, null);
            Thread.Sleep(timeBetweenSends);
            runReceiveScoreCallback();
            player1.BeginSend("WORD stares\n", (e, o) => { }, null);
            Thread.Sleep(timeBetweenSends);
            runReceiveScoreCallback();
            player2.BeginSend("WORD HAPPY\n", (e, o) => { }, null);
            Thread.Sleep(timeBetweenSends);
            runReceiveScoreCallback();
            player1.BeginSend("WORD lv\n", (e, o) => { }, null);
            Thread.Sleep(timeBetweenSends);
            runReceiveScoreCallback();
            player1.BeginSend("WORD beEr\n", (e, o) => { }, null);
            Thread.Sleep(timeBetweenSends);
            runReceiveScoreCallback();
            player1.BeginSend("WORD sap\n", (e, o) => { }, null);
            Thread.Sleep(timeBetweenSends);
            runReceiveScoreCallback();
            player1.BeginSend("WORD natures\n", (e, o) => { }, null);
            Thread.Sleep(timeBetweenSends);
            runReceiveScoreCallback();
            player1.BeginSend("TA'sAreCool\n", (e, o) => { }, null);
            Thread.Sleep(timeBetweenSends);
            runReceiveScoreCallback();
            player2.BeginSend("WORD Aardvark King\n", (e, o) => { }, null);
            runReceiveScoreCallback();
            Thread.Sleep(timeBetweenSends);
            Thread.Sleep(timeBetweenSends);
            player1.BeginReceive(SummaryReceive, player1);
            player2.BeginReceive(SummaryReceive, player2);

            Thread.Sleep(8000);
            Assert.AreEqual("19", player1Score);
            Assert.AreEqual("1", player2Score);
        }

        private void runReceiveScoreCallback()
        {
            player1.BeginReceive(ScoreCallback, player1);
            player2.BeginReceive(ScoreCallback, player2);
        }

        /**
         * 
         *  VARIOUS CALLBACKS TO CHECK WHAT IS BEING RECEIVED BY THE SERVER
         * 
         * */
        private void StartReceive(String s, Exception e, Object payload) 
        {
            StringSocket player = (StringSocket)payload;
            if (player == player1)
                player1StartMessage = s;
            else
                player2StartMessage = s;
        }
        private void TimeReceive(String s, Exception e, Object payload)
        {
                StringSocket player = (StringSocket)payload;
                if (player == player1)
                    player1TimeMessage += s;
                else
                    player2TimeMessage += s;
        }

        private void SummaryReceive(String s, Exception e, Object payload)
        {
            StringSocket player = (StringSocket)payload;
            if (player == player1)
                player1SummaryMessage = s;
            else
                player2SummaryMessage = s;
        }

        private void IgnoringReceive(String s, Exception e, Object payload)
        {
            Assert.AreEqual("IGNORING", s);
        }

        private void ScoreCallback(String s, Exception e, Object payload)
        {
                StringSocket player = (StringSocket)payload;
                if (s.StartsWith("TIME"))
                    return;
                else if (s == "IGNORING")
                    return;
                else if (s.StartsWith("SCORE ") && player == player1)
                {
                    player1Score = s.Substring(6, s.Length - 8);
                    player2Score = s.Substring(9, 1);
                }
           
        }

        /// <summary>
        /// Test for insuring the BoggleServer can handle different console arguments.
        /// 
        /// Each open window need to be closed by hand to finish the test!
        /// 
        /// </summary>
        [TestMethod]
        public void CommandLineArgumentsTest()
        {
            Process process = new Process();
            process.StartInfo.FileName = "BoggleServer.exe";
            process.StartInfo.Arguments = "2 dictionary.txt";
            process.Start();
            process.WaitForExit();

            Process process2 = new Process();
            process2.StartInfo.FileName = "BoggleServer.exe";
            process2.StartInfo.Arguments = "2 dictionary.txt THISISATESTCOOLL";
            process2.Start();
            process2.WaitForExit();
        }
    }
}

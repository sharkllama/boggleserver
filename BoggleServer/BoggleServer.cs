﻿/// BoggleServer implementation completed by:
///         Scott Wells and Matthew Mendez
/// Class:  CS3500 at the University of Utah
/// Date:   November 24, 2014         

using BB;
using CustomNetworking;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Boggle
{
    /*****************************************************************************************************
     ***************************************** GAME SERVER CODE ******************************************
     *****************************************************************************************************/
    /// <summary>
    /// This BoggleServer contains and runs all of the necessary functionality to host multiple Boggle matches between outside connecting clients.
    /// Players are put together in a match in the order that they connect to this server i.e. the first 2 clients to connect are paired, 
    /// the second two clients, etc.
    /// 
    /// When run from the command prompt, this server is defaulted to listening for connections only on port 2000.
    /// 
    /// </summary>
    public class BoggleServer
    {
        // TcpListener for listening to incoming connections
        private TcpListener server;

        // TcpListener for listening to web browser requests
        private TcpListener webServer;

        // Player that is waiting for a contender
        private Player waitingPlayer1;

        // A list containing all of the games currently being played on this server
        private List<Game> games;

        // All players connected and playing on this server
        private List<Player> allPlayers;

        // Dictionary containing all of the legal words that may be played on the board
        private HashSet<string> dictionary;
        
        // Set time limit for all games played on this server to abide by
        public readonly int gameTime;

        // Specified game board to play for all boggle games played on this server
        private string gameBoard;

        /// <summary>
        /// Main function sets up the server to start listening for incoming players to start playing Boggle.
        /// The default port this server listens to is port 2000.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //set up arguments to handle 
            //MANDATORY args[0] should be number of seconds that each Boggle game should last.
            int tempSeconds = Int32.Parse(args[0]);

            //MANDATORY args[1] should be the pathname of the dictionary legal word file
            string tempPathName = args[1];

            //OPTIONAL args[2] should be a string consisting of exactly 16 letters for the initial state of the Boggle board for each game, otherwise assume random board
            if (args.Length == 3)
                new BoggleServer(tempSeconds, tempPathName, args[2], 2000);
            else
                new BoggleServer(tempSeconds, tempPathName, null, 2000);

            //set up server debug window to provide logs of activity happening on the server
            Console.Read();
        }

        /// <summary>
        /// Builds and returns the collection of legal words into a dictionary for the game using the formatted text file path in the parameter. 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public HashSet<string> buildDictionaryFromFile(String filename)
        {
            HashSet<string> result = new HashSet<string>();
            using (StreamReader reader = new StreamReader(filename))
            {
                while (reader.Peek() >= 0)
                {
                    result.Add(reader.ReadLine().ToUpper());
                }
                return result;
            }
        }

        /// <summary>
        /// Instantiates an instance of this boggle server. Once the server has been created, it immediately starts
        /// listening for incoming connections.
        /// </summary>
        /// <param name="_gameTime">game time limit for all games</param>
        /// <param name="_filename">file to build the legal word dictionary from</param>
        /// <param name="_gameBoard">string of 16 chars that make up the board for the game</param>
        /// <param name="port">port to listen for incoming connections</param>
        public BoggleServer(int _gameTime, String _filename, String _gameBoard, int port)
        {
            dictionary = buildDictionaryFromFile(_filename);

            gameTime = _gameTime;

            gameBoard = _gameBoard;

            server = new TcpListener(IPAddress.Any, port);

            webServer = new TcpListener(IPAddress.Any, 2500);

            waitingPlayer1 = null;

            allPlayers = new List<Player>();
            games = new List<Game>();

            // Game server starts listening for connections...
            server.Start();
            server.BeginAcceptSocket(ConnectionReceived, null);

            // Web server starts listening for connections...
            webServer.Start();
            webServer.BeginAcceptSocket(WebConnectionReceived, null);
        }

        /// <summary>
        /// Callback dealing with connecting clients. This method sets the first connecting socket as player1 and waits
        /// for another socket to connect to become player2 for a match. Once player2 has connected, a match is created.
        /// </summary>
        private void ConnectionReceived(IAsyncResult ar)
        {
            // Shake hands
            Socket socket = server.EndAcceptSocket(ar);
            StringSocket ss = new StringSocket(socket, UTF8Encoding.Default);

            // If no players are already waiting for a match, simply create a new player.
            if (waitingPlayer1 == null)
            {
                waitingPlayer1 = new Player(ss);
                allPlayers.Add(waitingPlayer1);
                Console.WriteLine("First player connected");
            }
            // Otherwise this connecting socket is player 2, and a game is ready to be started.
            else
            {
                Player newPlayer = new Player(ss);
                allPlayers.Add(newPlayer);
                Game newGame = new Game(waitingPlayer1, newPlayer, this);

                //reset the waiting player
                waitingPlayer1 = null;
                games.Add(newGame);

                Console.WriteLine("Second player connected");
                // Begin waiting for players to send in their name, motioning that they are ready to play.
                ThreadPool.QueueUserWorkItem(x => newGame.Start());
            }

            // Initiate the routine to collect the name of this player
            ss.BeginReceive(NameReceived, ss);

            // Always listen for more connection requests
            server.BeginAcceptSocket(ConnectionReceived, null);
        }

        /// <summary>
        /// Callback dealing with assigning the player their name received to the corresponding socket it came from.
        /// If the connected user does not enter in PLAY before their name, the command is ignored and  the message "IGNORING"
        /// is sent to the client.
        /// </summary>
        /// <param name="name">name to be set for the player</param>
        /// <param name="e"></param>
        /// <param name="associatedStringSocket">payload marker to know what socket to assign the name to</param>
        private void NameReceived(String name, Exception e, Object associatedStringSocket)
        {
            // Read in the name from the client when they use the word PLAY before their name
            if (name.StartsWith("PLAY "))
            {
                name = name.Substring(5, name.Length - 5);
                // Give the player it's name
                foreach (Player p in allPlayers)
                    if (!p.ReadyToPlay && p.Socket == associatedStringSocket)
                        p.Name = name;
                Console.WriteLine("One of the players name is " + name);
            }

            // The user did NOT enter in PLAY at this point. Inform the client that the server is currently ignoring
            // and listen again for a correct PLAY command.
            else
            {
                StringSocket ss = (StringSocket)associatedStringSocket;
                ss.BeginSend("IGNORING\n", (ex, o) => { }, null);
                ss.BeginReceive(NameReceived, associatedStringSocket);
            }
        }

        /*****************************************************************************************************
         ***************************************** WEB SERVER CODE *******************************************
         *****************************************************************************************************/
        // Database connection data.
        private const string connectionString = "server=atr.eng.utah.edu;database=******;uid=******;password=******"; // Database credentials omitted for code demonstration 2/23/2015

        // The first half of the html sent to the requesting browser. HTML is built between the first and second half depending on DB information.
        private const string firstHalf = "<!DOCTYPE html><html><head><meta charset=\"utf-8\" /><title>Boggle</title><style>body{background-color: DarkSlateGray; }" + 
                                         "table, th, td {border: 3px solid DarkSlateGray;border-collapse: collapse;text-align:center; color: DarkSlateGray;}th, td {" + 
                                         "padding: 5px;text-align: left;}table#t01 {background-color: DarkSeaGreen;} table#board{width:400px;height:400px;" +
                                         "background-color: #f1f1c5;font-size:500%;}</style></head><body><h1>";

        // The second half of the html sent to the requesting browser. HTML is built between the first and second half depending on DB information.
        private const string secondHalf = "</h1></body></html>\r\n";

        // The html error message sent when the browser sends a syntactically incorrect message.
        private const string errorMessage = "<font color = \"White\"><h1>ERROR!</h1><p>Error fulfilling your request. You may use:<br></br> <b>/players</b> to " + 
                                            "see information about ALL games<br></br><b>/games?player=[<i>Player_Name</i>]</b> to see a specific players game " + 
                                            "statistics<br></br><b>/game?id=[<i>Game_ID</i>]</b> to view a specific game's information<br></br></p></font>";

        /// <summary>
        /// Connection callback for when a browser attempts to connect to port 2500 on the server's host machine.
        /// </summary>
        private void WebConnectionReceived(IAsyncResult ar)
        {
            // Shake hands with the browser.
            Socket socket = server.EndAcceptSocket(ar);
            StringSocket ss = new StringSocket(socket, UTF8Encoding.Default);

            // Send the browser messages to specify that html data will be sent to it
            Console.WriteLine("WEB CONNECTION");
            ss.BeginSend("HTTP/1.1 200 OK\r\n", (e, o) => { }, null);
            ss.BeginSend("Connection: close\r\n", (e, o) => { }, null);
            ss.BeginSend("Content-Type: text/html; charset=UTF-8\r\n", (e, o) => { }, null);
            ss.BeginSend("\r\n", (e, o) => { }, null);

            // Begin listening for messages (which will be the domain name extensions from the browser)
            ss.BeginReceive(sendDBData, ss);

            // Begin listening for more connections.
            webServer.BeginAcceptSocket(WebConnectionReceived, null);
        }

        /// <summary>
        /// Receive callback for when the browser sends a message to the server. Either a page of
        /// html that represents certain database data will be sent back to the browser, or an html
        /// error message will be sent. This all depends on the message received from the browser.
        /// </summary>
        private void sendDBData(string s, Exception e, object payload)
        {
            if (s == null)
                return;

            // The StringSocket was passed as the payload in "WebConnectionReceived"
            StringSocket ss = (StringSocket)payload; // payload is a string socket

            // Split string up
            string[] arr = s.Split(null);

            // The protocol of communication should be in the correct format or an error message is sent:
            if (arr[0] == "GET" && arr[2] == "HTTP/1.1")
            {
                // When "/players" is received, the browser is requesting information about all players.
                if (arr[1].StartsWith("/players"))
                {
                    ss.BeginSend(firstHalf + ConstructAllPlayersHTML() + secondHalf, (ex, o) => { ss.Close(); }, null);
                }
                // When "/games?player= "PlayerName"" is received, the browser is requesting information abouta  specific player.
                else if (arr[1].StartsWith("/games?player="))
                {
                    ss.BeginSend(firstHalf + ConstructPlayerHTML(arr[1].Substring(14)) + secondHalf, (ex, o) => { ss.Close(); }, null);
                }
                // When "/game?id= "ID"" is received, the browser is requesting information about a specific game.
                else if (arr[1].StartsWith("/game?id="))
                {
                    ss.BeginSend(firstHalf + ConstructGameInfoHTML(arr[1].Substring(9)) + secondHalf, (ex, o) => { ss.Close(); }, null);
                }
                // Or else there was a syntactical error and an error message is sent.
                else
                {
                    ss.BeginSend(firstHalf + errorMessage + secondHalf, (ex, o) => { ss.Close(); }, null);
                }
            }
            else { ss.BeginSend(firstHalf + errorMessage + secondHalf, (ex, o) => { ss.Close(); }, null); }
        }

        /// <summary>
        /// Sends DB information for a specified game p in the form of html.
        /// </summary>
        private string ConstructGameInfoHTML(string p)
        {
            int GameID = Int32.Parse(p);

            // Page header that displays which game is being viewed.
            string html = "<h1><font color = \"white\" size=\"7\">Game " + p + " Statistics</font></h1><table id=\"t01\">" + "<th>Player 1 Name</th>" +
                          "<th>Player 1 Score</th><th>Player 2 Name</th><th>Player 2 Score</th><th>Date and Time</th></tr>";

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                try
                {
                    // Open a connection
                    conn.Open();

                    // Create a command
                    MySqlCommand command = conn.CreateCommand();
                    command.CommandText = "select * from Games natural join GameSummaryInfo where ID =" + p;

                    // Execute the command and cycle through the DataReader object
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        // Must be in the form of the loop incase the ID does not exist. If it does not exist no information is shown.
                        while (reader.Read())
                        {
                            // Extract all necessary data from the database.
                            string player1Name = reader["Player1Name"].ToString();
                            string player2Name = reader["Player2Name"].ToString();
                            int player1Score = Int32.Parse(reader["Player1Score"].ToString());
                            int player2Score = Int32.Parse(reader["Player2Score"].ToString());
                            string dateTime = reader["GameTime"].ToString();
                            string boggleBoard = reader["Board"].ToString();
                            string player1LegalWords = reader["Player1LegalWords"].ToString();
                            string player2LegalWords = reader["Player2LegalWords"].ToString();
                            string commonWords = reader["CommonWords"].ToString();
                            string player1IllegalWords = reader["Player1IllegalWords"].ToString();
                            string player2IllegalWords = reader["Player2IllegalWords"].ToString();
                            
                            // The simple data for the game including player names, time played, etc. are constructed
                            // into a table at the beggining of the page:
                            html += "<td><a href = \"/games?player=" + player1Name + "\">" + player1Name + "</a></td>";
                            html += "<td>" + player1Score + "</td>";
                            html += "<td><a href = \"/games?player=" + player2Name + "\">" + player2Name + "</a></td>";
                            html += "<td>" + player2Score + "</td>";
                            html += "<td>" + dateTime + "</td>";
                            html += "</table>";
                            html += "<p></p>";

                            // Construct the table that represents the Boggle Board for the specified game.
                            html += "<font size =\"5\" color=\"white\">Boggle Board</font><table id=\"board\"><tr>";
                            for (int i = 0; i < 16; i++)
                            {
                                html += "<td><center>" + boggleBoard[i] + "</center></td>";
                                if ((i + 1) % 4 == 0)
                                    html += "</tr><tr>";
                            }

                            // Begin to construct the game summary table.
                            html += "</tr></table><p></p><font size =\"5\" color=\"white\">Words Played</font><table id=\"t01\"><tr><th>" + player1Name + "'s Legal</th><th>"
                                    + player2Name + "'s Legal</th><th>Common</th><th>" + player1Name + "'s Illegal</th><th>" + player2Name + "'s Illegal</th><tr><td>";

                            // All necessary lists (player1LegalWords, player2LegalWords, commonWords, player1IllegalWords, player2IllegalWords) that are
                            // part of the game summary are properly constructed into the game summary table.
                            foreach (string s in player1LegalWords.Split(null))
                            {
                                html += s + "<br>";
                            }
                            html += "</td><td>";

                            foreach (string s in player2LegalWords.Split(null))
                            {
                                html += s + "<br>";
                            }
                            html += "</td><td>";

                            foreach (string s in commonWords.Split(null))
                            {
                                html += s + "<br>";
                            }
                            html += "</td><td>";

                            foreach (string s in player1IllegalWords.Split(null))
                            {
                                html += s + "<br>";
                            }
                            html += "</td><td>";

                            foreach (string s in player2IllegalWords.Split(null))
                            {
                                html += s + "<br>";
                            }
                            html += "</td></tr></table>";
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                return html;
            }
        }

        /// <summary>
        /// Sends DB information for a specified player in the form of html.
        /// </summary>
        private string ConstructPlayerHTML(string player)
        {
            // Handle the case when a space is included in a player name typed in the address bar of the browser.
            player = player.Replace("%20", " ");

            // Page header that specifies which player information is being displayed about.
            string html = "<font color = \"white\" size=\"7\">Games Played by " + player + "</font><table id=\"t01\"><p></p>" + "<tr><th>Game ID</th><th>Date and Time</th>" +
                          "<th>Opponent</th><th>" + player + "'s Score</th><th>Opponents Score</th></tr>";

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                try
                {
                    // Open a connection
                    conn.Open();

                    // Create a command
                    MySqlCommand command = conn.CreateCommand();
                    command.CommandText = "select * from Games natural join GameSummaryInfo ORDER by GameTime DESC";

                    // Execute the command and cycle through the DataReader object
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        // Each game is looped through.
                        while (reader.Read())
                        {
                            // Extract all necessary information from the database.
                            int gameID = Int32.Parse(reader["ID"].ToString());
                            string player1Name = reader["Player1Name"].ToString();
                            string player2Name = reader["Player2Name"].ToString();
                            int player1Score = Int32.Parse(reader["Player1Score"].ToString());
                            int player2Score = Int32.Parse(reader["Player2Score"].ToString());
                            string dateTime = reader["GameTime"].ToString();

                            // Differentiate between which player is which and display the correct information
                            // about both players under the correct names.
                            if (player1Name == player)
                            {
                                html += "<tr><td><a href = \"/game?id=" + gameID + "\">" + gameID + "</a></td>";
                                html += "<td>" + dateTime + "</td>";
                                html += "<td><a href = \"/games?player=" + player2Name + "\">" + player2Name + "</a></td>";
                                html += "<td>" + player1Score + "</td>";
                                html += "<td>" + player2Score + "</td></tr>";
                            }
                            else if (player2Name == player)
                            {
                                html += "<tr><td><a href = \"/game?id=" + gameID + "\">" + gameID + "</a></td>";
                                html += "<td>" + dateTime + "</td>";
                                html += "<td><a href = \"/games?player=" + player1Name + "\">" + player1Name + "</a></td>";
                                html += "<td>" + player2Score + "</td>";
                                html += "<td>" + player1Score + "</td></tr>";
                            }
                        }
                        html += "</table>";
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                return html;
            }
        }

        /// <summary>
        /// Sends DB information for all players in the form of html.
        /// </summary>
        private string ConstructAllPlayersHTML()
        {
            // This is used to easily map wins, losses, and ties to a player's name.
            Dictionary<string, int[]> playerInfo = new Dictionary<string, int[]>();

            // Used to easily index the dictionary's array
            int wins = 0;
            int losses = 1;
            int ties = 2;

            // Construct the table header.
            string html = "<font color = \"white\" size=\"7\">Boggle Player Roster</font><p></p><table id=\"t01\"><tr><th>Player Name</th><th>Wins</th><th>Losses</th><th>Ties</th></tr>";

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                try
                {
                    // Open a connection
                    conn.Open();

                    // Create a command
                    MySqlCommand command = conn.CreateCommand();
                    command.CommandText = "select * from Games natural join GameSummaryInfo order by GameTime DESC";
                    
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        // Loop through every game and organize players and their wins, losses, and ties into the dictionary.
                        while (reader.Read())
                        {
                            // Extract player names from the current game.
                            string player1Name = reader["Player1Name"].ToString();
                            string player2Name = reader["Player2Name"].ToString();

                            // Add players to the table if they are not already there
                            if (!playerInfo.ContainsKey(player1Name))
                                playerInfo.Add(player1Name, new int[3]);

                            if (!playerInfo.ContainsKey(player2Name))
                                playerInfo.Add(player2Name, new int[3]);

                            // Check scores for each player and increment the wins, losses, and ties for each player as needed
                            int player1Score = Int32.Parse(reader["Player1Score"].ToString());
                            int player2Score = Int32.Parse(reader["Player2Score"].ToString());
                            if (player1Score > player2Score)
                            {
                                playerInfo[player1Name][wins]++;
                                playerInfo[player2Name][losses]++;
                            }
                            else if (player1Score < player2Score)
                            {
                                playerInfo[player2Name][wins]++;
                                playerInfo[player1Name][losses]++;
                            }
                            else
                            {
                                playerInfo[player1Name][ties]++;
                                playerInfo[player2Name][ties]++;
                            }
                        }

                        // Properly construct a row for each player on the html player.
                        foreach (string player in playerInfo.Keys)
                        {
                            html += "<tr><td><a href = \"/games?player=" + player + "\">" + player + "</a></td>";
                            html += "<td>" + playerInfo[player][wins] + "</td>";
                            html += "<td>" + playerInfo[player][losses] + "</td>";
                            html += "<td>" + playerInfo[player][ties] + "</td></tr>";
                        }
                        html += "</table>";
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                return html;
            }
        }

        /*****************************************************************************************************
         ***************************************** GAME CLASS ************************************************
         *****************************************************************************************************/
        /// <summary>
        /// This private Game class is a container for all of the information needed to conduct a Boggle
        /// match between 2 players connected to a BoggleServer.
        /// </summary>
        private class Game
        {
            // Reference kept of the BoggleServer for the game to edit server values when necessary
            private BoggleServer server;

            // This game's BoggleBoard object used in the match.
            private BoggleBoard board;

            // The 2 players playing against each other.
            private Player Player1;
            private Player Player2;

            // Flag for identifying when the time has run out and the game is over
            private bool gameOver;

            // A collection of the common words used when sending game results at the end of the match.
            private HashSet<string> commonWords;

            // Flag used for when one of the clients close to notify the other player that the match is terminated.
            private bool clientIsClosed;

            /// <summary>
            /// Constructs a Boggle match given 2 players and the server this game is being played on.
            /// </summary>
            /// <param name="P1">First player</param>
            /// <param name="P2">Second player</param>
            /// <param name="server">reference to the server/param>
            public Game(Player P1, Player P2, BoggleServer server)
            {
                clientIsClosed = false;
                Console.WriteLine("Game Created");
                commonWords = new HashSet<string>();
                this.server = server;
                if (server.gameBoard == null)
                    board = new BoggleBoard();
                else
                    board = new BoggleBoard(server.gameBoard);
                Player1 = P1;
                Player2 = P2;
                gameOver = false;
            }

            /// <summary>
            /// Starts this Boggle match.
            /// </summary>
            public void Start()
            {
                //wait until both players have entered their names...
                while (!(Player1.ReadyToPlay && Player2.ReadyToPlay))
                {
                    Thread.Sleep(100);
                }

                Console.WriteLine("Game started");
                // Send Start command to the clients, if for any reason there is an exception (i.e. close), send terminated to the opposite player.
                Player1.Socket.BeginSend("START " + board.ToString() + " " + server.gameTime + " " + Player2.Name + "\n", (e, o) => {if(e != null)SendTerminated(Player2); }, null);
                Player2.Socket.BeginSend("START " + board.ToString() + " " + server.gameTime + " " + Player1.Name + "\n", (e, o) => { if (e != null)SendTerminated(Player1); }, null);

                // Start continually sending the time on a new Thread
                ThreadPool.QueueUserWorkItem(x => { SendTimeLeft();});

                // Start listening for words from each client!
                Player1.Socket.BeginReceive(ListenForWords, Player1);
                Player2.Socket.BeginReceive(ListenForWords, Player2);
            }

            /// <summary>
            /// This method keeps track of the time elapsed for this game. The seconds left are continually sent exactly 1 second apart to each client.
            /// Once the seconds have run out, this method invokes the end of the game routine. If at any time there is an error (i.e. closed client)
            /// TERMINATED is sent to the player still playing.
            /// </summary>
            public void SendTimeLeft()
            {
                    for (int timeLeft = server.gameTime; timeLeft >= 0; timeLeft--)
                    {
                        Thread.Sleep(1000);
                        if (clientIsClosed)
                            break;
                        Player1.Socket.BeginSend("TIME " + timeLeft + "\n", (e, o) => { if (e != null)SendTerminated(Player2); }, null);
                        Player2.Socket.BeginSend("TIME " + timeLeft + "\n", (e, o) => { if (e != null)SendTerminated(Player1); }, null);     
                    }
                    if(!clientIsClosed)
                        EndTheGame();
            }

            /// <summary>
            /// This method sends a TERMINATED message to the given player in the parameter. Once the  message has been received by the client, 
            /// the server closes the players socket and deletes both the players from the list of players currently on the server.
            /// </summary>
            /// <param name="player"></param>
            private void SendTerminated(Player player)
            {
                if (!clientIsClosed)
                {
                    player.Socket.BeginSend("TERMINATED", (e, o) => { player.Socket.Close(); }, null);
                    server.allPlayers.Remove(Player1);
                    server.allPlayers.Remove(Player2);
                    clientIsClosed = true;
                }
            }

            /// <summary>
            /// ListenForWords is a callback function that is called immediately after the game
            /// has started. The server will send the client "START . . .", and then beginReceive on 
            /// the stringSocket. This method's main function is to process a words sent from the
            /// client. It decides whether the word is legal, illegal, common, too short, or already
            /// on the users word list, and acts accordingly. It then begins listening for more words
            /// from the same client. 
            /// </summary>
            private void ListenForWords(String word, Exception e, Object AssociatedPlayer)
            {
                
                Player thisPlayer = (Player)AssociatedPlayer;
                Player otherPlayer;

                if (AssociatedPlayer == Player1)
                    otherPlayer = Player2;
                else
                    otherPlayer = Player1;

                // If something goes wrong with the receive, it is assumed that the connection
                // between the server and the client being received from is closed and 
                // the other player is notified.
                if (e != null || word == null)
                {
                    SendTerminated(otherPlayer);
                    return;
                }
                
                // If the game is over, the client should no longer be able to accumulate 
                // points, so they are completely igonored.
                if (!gameOver)
                {
                    if (word.StartsWith("WORD "))
                    {
                        // The word is processed to be an uppercase version of only the word that is
                        // being played with no new lines or carriage returns.
                        word = word.Substring(5, word.Length - 5).ToUpper();
                        word = word.Replace("\r", "");
                        word = word.Replace("\n", "");

                        Console.WriteLine("" + word + " received from " + thisPlayer.Name);

                        // If the word is too short, already contained in either of this client's word
                        // lists, or is already contained in the common words list, the word is ignored
                        // and the server begins listening for more. Otherwise it is added to the correct
                        // list: LegalWords, IllegalWords, or CommonWords.
                        if (word.Length < 3 || thisPlayer.LegalWords.Contains(word) || thisPlayer.IllegalWords.Contains(word) || commonWords.Contains(word))
                        {
                            thisPlayer.Socket.BeginReceive(ListenForWords, AssociatedPlayer);
                            return;
                        }
                        else if (otherPlayer.LegalWords.Contains(word))
                        {
                            commonWords.Add(word);
                            otherPlayer.LegalWords.Remove(word);
                        }
                        else if (server.dictionary.Contains(word) && board.CanBeFormed(word))
                        {
                            Console.WriteLine(word + " Legal Word");
                            thisPlayer.LegalWords.Add(word);
                        }
                        else
                        {
                            Console.WriteLine(word + " Illegal Word");
                            thisPlayer.IllegalWords.Add(word);
                        }
               
                        updateScores();

                        Console.WriteLine(Player1.Score);
                        Console.WriteLine(Player2.Score);

                        // The notable part of these statements is the callback function. If there is any problem 
                        // sending to either client, the other client is notified and closed off as well.
                        Player1.Socket.BeginSend("SCORE " + Player1.Score + " " + Player2.Score + "\n", (ex, o) => { if (e != null)SendTerminated(Player2); }, null);
                        Player2.Socket.BeginSend("SCORE " + Player2.Score + " " + Player1.Score + "\n", (ex, o) => { if (e != null)SendTerminated(Player1); }, null);
                        thisPlayer.Socket.BeginReceive(ListenForWords, AssociatedPlayer);
                    }
                    else
                    {
                        // If the sent string does not begin with "WORD " protocol has been broken.
                        thisPlayer.Socket.BeginSend("IGNORING\n", (ex, o) => { if (e != null)SendTerminated(otherPlayer); }, null);
                        thisPlayer.Socket.BeginReceive(ListenForWords, AssociatedPlayer);
                    }
                }
                else
                    thisPlayer.Socket.BeginSend("IGNORING\n", (ex, o) => { if (e != null)SendTerminated(otherPlayer); }, null);                
            }

            /// <summary>
            /// Updates the scores of each player in this game. Each player has
            /// their own legal word lists and illegal word list. Their scores
            /// are based on the words in these lists.
            /// </summary>
            private void updateScores()
            {
                int p1Score = 0;
                int p2Score = 0;

                // Accumulate Player1's points based on the length
                // of all words in their LegalWords list.
                foreach(string word in Player1.LegalWords)
                {
                     p1Score += getWordValue(word);
                     Console.WriteLine("LEGAL WORD POINTS ADDED");
                }

                // Subtract 1 point from Player1's points for each
                // word in their illegal words list. 
                foreach (string word in Player1.IllegalWords)
                {
                    p1Score--;
                    Console.WriteLine("ILLEGAL WORD POINTS SUBTRACTED");
                }

                // Repeat the process for player 2.
                foreach(string word in Player2.LegalWords)
                {
                     p2Score += getWordValue(word);
                     Console.WriteLine("LEGAL WORD POINTS ADDED");
                }
                foreach (string word in Player2.IllegalWords)
                {
                    p2Score--;
                    Console.WriteLine("ILLEGAL WORD POINTS SUBTRACTED");
                }

                Player1.Score = p1Score;
                Player2.Score = p2Score;
            }

            /// <summary>
            /// This method is called when the time for this game is up. It 
            /// sets the game over boolean, which disallows users to accumulate
            /// any more points, and sends the correct game summary to each 
            /// player.
            /// </summary>
            private void EndTheGame()
            {
                gameOver = true;
                String player1Summary = constructSummary(Player1);
                String player2Summary = constructSummary(Player2);

                // After the summaries are sent, the sockets must be closed and
                // the players must be removed from the players list. If there are
                // any problems with sending the summary to a client, the other player
                // is notified and disconnected.
                Player1.Socket.BeginSend(player1Summary, (e, o) => { if (e != null)SendTerminated(Player2); else Player1.Socket.Close(); }, null);
                Player2.Socket.BeginSend(player2Summary, (e, o) => { if (e != null)SendTerminated(Player1); else Player2.Socket.Close(); }, null);
                server.allPlayers.Remove(Player1);
                server.allPlayers.Remove(Player2);

                UpdateDatabase();
            }

            /// <summary>
            /// This method takes all information associated with the game and updates it
            /// to the database.
            /// </summary>
            private void UpdateDatabase()
            {
                // Connect to the DB
                using (MySqlConnection conn = new MySqlConnection(connectionString))
                {
                    try
                    {
                        // Open a connection
                        conn.Open();

                        // Create a command that updates both tables using all necessary information from the game.
                        MySqlCommand command = conn.CreateCommand();
                        command.CommandText = "INSERT INTO Games (Player1Name, Player1Score, Player2Name, Player2Score, GameTime, Board, GameLength) VALUES ('"
                                               + Player1.Name + "', " + Player1.Score + ", '" + Player2.Name + "', " + Player2.Score + ", '" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") +
                                               "', '" + board.ToString() + "', " + server.gameTime + ");";

                        command.CommandText += "INSERT INTO GameSummaryInfo (Player1LegalWords, Player2LegalWords, CommonWords, Player1IllegalWords, Player2IllegalWords) VALUES ('"
                                                + GetItemsFromList(Player1.LegalWords) + "', '" + GetItemsFromList(Player2.LegalWords) + "', '" + GetItemsFromList(this.commonWords.ToList())
                                               + "', '" + GetItemsFromList(Player1.IllegalWords) + "', '" + GetItemsFromList(Player2.IllegalWords) + "');";

                        // Execute the command
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }

            /// <summary>
            /// Returns a space-separated string of all items in the given list.
            /// </summary>
            private string GetItemsFromList(List<string> list)
            {
                string result = "";
                foreach (string s in list)
                    result += s + " ";
                return result;
            }

            /// <summary>
            /// This method constructs the game summary for a given player. The
            /// game summary is in the following format: STOP + thisPlayer Legal
            /// Word Count + List of thisPlayer's legal words + otherPlayer Legal
            /// Word Count + List of otherPlayer's legal words +  commonWord count 
            /// + List of Common Words + thisPlayer IllegalWord Count + thisPlayer
            /// IllegalWord list + otherPlayer IllegalWord Count + otherPlayer list
            /// of Illegal Words.
            /// </summary>
            private string constructSummary(Player player)
            {
                string result = "STOP ";
                Player otherPlayer;
                if (player == Player1)
                    otherPlayer = Player2;
                else
                    otherPlayer = Player1;

                result += player.LegalWords.Count + " ";
                foreach (string word in player.LegalWords)
                    result += word + " ";

                result += otherPlayer.LegalWords.Count + " ";
                foreach (string word in otherPlayer.LegalWords)
                    result += word + " ";

                result += commonWords.Count + " ";
                foreach (string word in commonWords)
                    result += word + " ";

                result += player.IllegalWords.Count + " ";
                foreach (string word in player.IllegalWords)
                    result += word + " ";

                result += otherPlayer.IllegalWords.Count + " ";
                foreach (string word in otherPlayer.IllegalWords)
                    result += word + " ";

                // Get rid of the final space from the preceding foreach loop.
                result = result.Substring(0, result.Length - 1);

                result += "\n";

                return result;
            }

            /// <summary>
            /// Depending on the length of the word, this method computes
            /// the score value of the word. 
            /// </summary>
            private int getWordValue(string word)
            {
                switch (word.Length)
	            {
		            case 3:
                    case 4:
                        return 1;
                    case 5:
                        return 2;
                    case 6:
                        return 3;
                    case 7:
                        return 5;
                    default:
                        return 11;
	            }
            }
        }

        /*****************************************************************************************************
         ***************************************** PLAYER CLASS **********************************************
         *****************************************************************************************************/
        /// <summary>
        /// A Player is an encapsulation of data that is
        /// associated with a Boggle Player. Each player
        /// is a socket with a name, score, list of Legal
        /// words, list of Illegal words, and is either
        /// ready to play, or not ready to play, depending
        /// on whether or not they have sent their name to
        /// the server.
        /// </summary>
        private class Player
        {
            StringSocket socket;
            string name;
            bool readyToPlay;
            int score;
            List<String> legalWords;
            List<String> illegalWords;

            /// <summary>
            /// Constructor for a player. A player is orginally
            /// not ready to play, has a score of 0, and has no 
            /// words in both lists. 
            /// </summary>
            public Player(StringSocket _socket)
            {
                this.socket = _socket;
                readyToPlay = false;
                score = 0;
                legalWords = new List<string>();
                illegalWords = new List<string>();
            }

            /// <summary>
            /// Property that allows the Player's name to be 
            /// get and set. When the Player's name is set, 
            /// it's "Ready to Play" status is set to true.
            /// </summary>
            public string Name
            {
                get { return this.name; }
                set
                {
                    this.name = value;
                    readyToPlay = true;
                }
            }

            /// <summary>
            /// Property that allows the Player's "Ready to
            /// Play" status to be get.
            /// </summary>
            public bool ReadyToPlay
            {
                get { return readyToPlay; }
            }

            /// <summary>
            /// Property that allows the Player's socket to
            /// be get.
            /// </summary>
            public StringSocket Socket
            {
                get { return socket; }
            }

            /// <summary>
            /// Property that allows the Player's score
            /// to be get and set.
            /// </summary>
            public int Score
            {
                get { return score; }
                set { score = value;}
            }

            /// <summary>
            /// Property that allows the Player's list of
            /// Legal words to get got.
            /// </summary>
            public List<string> LegalWords
            {
                get { return legalWords; }
            }

            /// <summary>
            /// Property that allows the Player's list of
            /// Illegal words to get got.
            /// </summary>
            public List<string> IllegalWords
            {
                get { return illegalWords; }
            }
          }
        }
    }


    

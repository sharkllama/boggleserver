﻿Program Authors: Scott Wells & Matthew Mendez

/*****************************************************************************************
 ************************************ PS10 README ****************************************
 *****************************************************************************************/
 Database Documentation:
 Our database consists of two tables, both of which use the auto-incremented primary key "ID" (GameID):
 
 // The following "Games" table encapsulates information related to each game. 
 1. Games - ID, Player1Name, Player1Score, Player2Name, Player2Score, GameTime (when the game was played), 
		   Board, GameLength

 // The following "GameSummaryInfo" tables encapsulates game summary information related to each game.
 2. GameSummaryInfo - ID, Player1LegalWords, Player2LegalWords, CommonWords, Player1IllegalWords, Player2IllegalWords

 
 We used several queries to insert and retrieve information from our database. They include the following:
 // The following query is used to retrieve information from the DB about a specific game.
 // This information is displayed when /game?id="ID" is sent from the browser.
 1."select * from Games natural join GameSummaryInfo where ID =" + p 
 
 // The following query is used to retrieve information from the DB about all games in 
 // order of most recently played.
 // This information is used to display the page requested by /games?player="PlayerName".
 // It is also used to display the page requested by /players.
 2."select * from Games natural join GameSummaryInfo order by GameTime DESC"

 //The following query is used to update the GameSummaryInfo table with all necessary info when a game is over.
 3."INSERT INTO GameSummaryInfo (Player1LegalWords, Player2LegalWords, CommonWords, Player1IllegalWords, Player2IllegalWords) VALUES ('"
    + GetItemsFromList(Player1.LegalWords) + "', '" + GetItemsFromList(Player2.LegalWords) + "', '" + GetItemsFromList(this.commonWords.ToList())
    + "', '" + GetItemsFromList(Player1.IllegalWords) + "', '" + GetItemsFromList(Player2.IllegalWords) + "');";
 
 // The following query is used to update the Games table with all necessary info when a game is over.
 4."INSERT INTO Games (Player1Name, Player1Score, Player2Name, Player2Score, GameTime, Board, GameLength) VALUES ('"
	+ Player1.Name + "', " + Player1.Score + ", '" + Player2.Name + "', " + Player2.Score + ", '" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") +
	"', '" + board.ToString() + "', " + server.gameTime + ");";

 MORE INFORMATION REGARDING PS10:
 We ran into ultimately no issues while working on this assignment. It took us about an hour to learn how to use the database
 properly, but once we figured it out, the program fell out of our fingers. The most trouble we had other than the database was 
 properly formatting the website document data using CSS. Also, all of the work that we did on 12/11 was not committed to the
 repository nor saved to the C: drive, so about 2 hours of work had to be repeated (it mainly consisted of commenting and writing
 this README file.).

/*****************************************************************************************
 ************************************ PS8 README *****************************************
 *****************************************************************************************/
Design Decisions:
11/18/14: We were not sure whether to create a game between two players after they have both connected or after they have both sent in their name. 
The assignment specifications states that “once the server has received connections from two clients that are ready to play, it pairs them in a game”,
which convinced us to create the game immediately when both players are connected.
UPDATE: Matthew Turner has now unofficially stated that we should not create the game until both players have sent in their names. We are going to 
stick with our original implementation unless this becomes an official specification.

11/20/14: The assignment specifies how Boggle scoring works, but does not say how we should handle it when the game is ongoing. We were not sure 
whether to accumulate points for all words that the users enter and deduct the points for “common words” once the game is over, or to deduct points 
for common words when the game is ongoing. We decided for the latter. Scoring will be updated and sent to the users for all cases (common words, 
legal words, illegal words) when the game is ongoing.

Problems:
11/20/14: Today we finished up the base requirements for the server and began attempting to connect to it using telnet. The connections were working 
great, but once our server started sending out time updates, we were consistently getting NullReferenceExceptions. It took us a reasonable amount of 
time to figure out what the cause of these exceptions were: we were passing null through the callback parameter of the beginSend function when sending 
time updates. We thought it would be fine to pass null since we didn’t want anything to happen after the time was sent, but we needed to pass (e, o) => {}, 
which was the fix to our problem. We noticed that there were multiple other places where we passed null as the callback and fixed those as well. 

11/21/14: We have been constantly getting an ObjectDisposedException when attempting to close one client at any point during the Boggle server connection. 
It seems that the server is attempting to send more messages to the closed socket after we close the client, and that the exception is a result of this. 
We have attempted putting try-catch statements in multiple places to avoid this error, but have had no success. It seems that the ObjectDisposedException 
is not catchable. We first attempted to catch all possible exceptions: catch(Exception), which didn’t work so we figured that ObjectDisposedException is 
not a sub-exception under the Exception hierarchy and that it is a special exception. With this in mind, we attempted catching only the ObjectDisposedException 
which also failed. The MSDN documentation shows that ODE are indeed a subclass of Exception so there needs to be some deeper issue causing our heartache.
UPDATE: Closing a client while the game is in progress sends terminated to its matched player but it currently crashes the server due to the ObjectDisposedException.

Testing:
11/22/14: Testing has so far been the greatest of our difficulties. First of all, we did not know how to begin running the server. We thought creating a new 
thread that ran BoggleServer.Main with a passed string[] argument would be sufficient, but this behavior is not allowed. We decided that simply creating a new 
BoggleServer object in our tests was good enough, even though our code coverage would not be 100% since Main would not be tested. After figuring this out, we 
had much trouble figuring out exactly how to simulate the behavior of a client connecting to the server once it is running. We began to make a new “Testing Only” 
constructor where we could specify which sockets to connect to right off the bat, but then realized that we would only be testing our testing if we took this 
approach. After about a half-hour of research, we learned how to use a TcpClient to simulate a connecting socket to the server. This is what we used to carry 
out the rest of our tests.